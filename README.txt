
Programming the STM32F030F4 part on the Hylands board is facilitated
using the JLinkExe program provided by Segger.  Currently in use is
JLink_Linux_V618d_x86_64.

The procedure is:
    1) prepare the elf file for Jlink by using objcopy.
    	arm-none-eabi-objcopy -O ihex XXX.elf XXX.hex will convert the
	elf file to a suitable hex file.
    2) execute JLinkExe
    	o J-Link> usb
	o J-Link> si swd
	o J-Link> device cortex-m0
	o J-Link> speed 500
	o J-Link> connect
	o J-Link> loadfile XXX.hex
    3) That's it!  The PIC code should now indicate no problems.

