/**
  ******************************************************************************
  * @file
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    27-May-2016
  * @brief   Header for main.c module\n
  *          from: Examples_LL/ADC/ADC_MultiChannelSingleConversion/Inc/main.h
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "stm32f0xx.h"
#include "stm32f0xx_ll_bus.h"
#include "stm32f0xx_ll_rcc.h"
#include "stm32f0xx_ll_system.h"
#include "stm32f0xx_ll_utils.h"
#include "stm32f0xx_ll_cortex.h"
#include "stm32f0xx_ll_gpio.h"
#include "stm32f0xx_ll_exti.h"
#include "stm32f0xx_ll_adc.h"
#include "stm32f0xx_ll_dma.h"
#include "stm32f0xx_ll_spi.h"
#include "stm32f0xx_ll_tim.h"
#include "stm32f0xx_ll_usart.h"
#if defined(USE_FULL_ASSERT)
#include "stm32_assert.h"
#endif /* USE_FULL_ASSERT */
#include "stm32f0xx_flash.h"

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _MAIN_H_
#define _MAIN_H_

extern void FLASH_Unlock(void);
extern void FLASH_Lock(void);
extern void FLASH_OB_Unlock(void);
extern void FLASH_OB_Lock(void);
extern void FLASH_OB_Launch(void);
extern FLASH_Status FLASH_OB_BOOTConfig(uint8_t OB_BOOT1);

/* Exported types ----------------------------------------------------------*/
/* Exported constants ------------------------------------------------------*/

/**
 * ADC channels enabled
 */
enum {
    CH0 = 96,   /**< was/is the azimuth */
    CH1 = 100,  /**< was/is the RFSSI sensor */
    TEMP = 104, /**< built-in to take temperature samples */
    VREF = 108  /**< also built in - currently not used */
};

/**
 * it may be beneficial to oversample the ADC readings
 */
#define OVRSAMPLE_LEN   8       /**< oversample depth */
struct adc_s_val {
    uint16_t    total;          /**< if the oversampling occurs locally */
    uint16_t    idx;            /**< next value to store/delete */
    uint16_t    vals[OVRSAMPLE_LEN]; /**< oversample storage */
};

/** Exported macro --------------------------------------------------------*/

/* Define used to enable time-out management*/
#define USE_TIMEOUT       0

/**
 * Using a combination of writes directly to the SPI device and using
 * the DMA to send the valid data works at the present data rates.
 * \bNOTE:\n
 *  At present the non-DMA path is not well maintained.
 */
#define SPI_DMA_ENB

#define BUILD_DATE      __DATE__
#define BUILD_TIME      __TIME__

#define SW_RELEASE      0x5101  /**< this is two 8 bit numbers, the MSB
                                 * indicates the major version, LSB is the
                                 * minor - ie: 00.01 is 0001 */

//#define USE_USART

/** Exported functions ---------------------------------------------------- */

extern void Configure_SPI_DMA(void);
extern void Configure_SPI(void);
extern void Activate_SPI(void);

extern void Configure_USART(void);
extern void Configure_USART_DMA(void);

extern void Configure_DMA(void);
extern void Configure_ADC(void);
extern void Activate_ADC(void);
extern void Configure_TIM_TimeBase_ADC_trigger(void);

#endif /* __MAIN_H */

/********************** (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
