/**
  ******************************************************************************
  * @file
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    27-May-2016
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and
  *          peripherals interrupt service routine.
  *          from:
  *          Examples_LL/ADC/ADC_MultiChannelSingleConversion/Src/stm32f0xx_it.c
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_it.h"

/** @addtogroup STM32F0xx_LL_Examples
  * @{
  */

/** @addtogroup ADC_MultiChannelSingleConversion
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M0 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
    /* Go to infinite loop when Hard Fault exception occurs */
    while(1) {
    }
}


/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}


/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
volatile uint32_t systime;
void SysTick_Handler(void)
{
    //LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_1);
    ++systime;
    //LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_1);
}

extern void AdcDmaTransferComplete_Callback(void);
extern void AdcDmaTransferError_Callback(void);
extern void AdcGrpRegularSequenceConvComplete_Callback(void);
extern void AdcGrpRegularOverrunError_Callback(void);
#ifdef SPI_DMA_ENB
extern void SPI1_DMA1_ReceiveComplete_Callback(void);
extern void SPI1_DMA1_TransmitComplete_Callback(void);
extern void SPI1_TransferError_Callback(void);
#else
extern void SpiRxAndAct(void);
extern void SpiTxMt(void);
extern void SpiErrFlag(void);
#endif

/******************************************************************************/
/*                 STM32F0xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f0xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles ADC1 interrupt request.
  * @param  None
  * @retval None
  */
void ADC1_IRQHandler(void)
{
    /* Check whether ADC group regular end of sequence conversions caused
     * the ADC interruption. */
    if(LL_ADC_IsActiveFlag_EOS(ADC1) != 0) {
        //LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_1);
        /* Clear flag ADC group regular end of sequence conversions */
        LL_ADC_ClearFlag_EOS(ADC1);

        /* Call interruption treatment function */
        AdcGrpRegularSequenceConvComplete_Callback();
        //LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_1);
    }

    /* Check whether ADC group regular overrun caused the ADC interruption */
    if(LL_ADC_IsActiveFlag_OVR(ADC1) != 0) {
        /* Clear flag ADC group regular overrun */
        LL_ADC_ClearFlag_OVR(ADC1);

        /* Call interruption treatment function */
        AdcGrpRegularOverrunError_Callback();
    }
}

/**
 * @brief  This function handles DMA1 - Channel 1 interrupt request.
 * @param  None
 * @retval None
 */
void DMA1_Channel1_IRQHandler(void)
{
    /* Check whether DMA transfer complete caused the DMA interruption */
    if(LL_DMA_IsActiveFlag_TC1(DMA1) == 1) {
        /* Clear flag DMA global interrupt */
        /* (global interrupt flag: half transfer and transfer complete flags) */
        //LL_GPIO_SetOutputPin(GPIOF, LL_GPIO_PIN_1);

        LL_DMA_ClearFlag_GI1(DMA1);

        /* Call interruption treatment function */
        AdcDmaTransferComplete_Callback();
        //LL_GPIO_ResetOutputPin(GPIOF, LL_GPIO_PIN_1);
    }

    /* Check whether DMA transfer error caused the DMA interruption */
    if(LL_DMA_IsActiveFlag_TE1(DMA1) == 1) {
        /* Clear flag DMA transfer error */
        LL_DMA_ClearFlag_TE1(DMA1);

        /* Call interruption treatment function */
        AdcDmaTransferError_Callback();
    }
}

/**
 * @brief  This function handles DMA1 - Channel 2&3 interrupt request.
 *         Used by the SPI transmit and receive functions.
 * @param  None
 * @retval None
 */
void DMA1_Channel2_3_IRQHandler(void)
{
#ifdef SPI_DMA_ENB
    /* channel 2 is the receive side */
    if(LL_DMA_IsActiveFlag_TC2(DMA1)) {
        //LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_1);
        LL_DMA_ClearFlag_GI2(DMA1);
        /* Call function Reception complete Callback */
        SPI1_DMA1_ReceiveComplete_Callback();
        //LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_1);
    }
    if(LL_DMA_IsActiveFlag_TC3(DMA1)) {
        LL_DMA_ClearFlag_GI3(DMA1);
        /* Call function Transmission complete Callback */
        SPI1_DMA1_TransmitComplete_Callback();
    }
    if(LL_DMA_IsActiveFlag_TE2(DMA1)) {
        LL_DMA_ClearFlag_TE2(DMA1);
        /* Call Error function */
        SPI1_TransferError_Callback();
    } else if(LL_DMA_IsActiveFlag_TE3(DMA1)) {
        LL_DMA_ClearFlag_TE3(DMA1);
        /* Call Error function */
        SPI1_TransferError_Callback();
    }
#endif
}

void SPI1_IRQHandler(void)
{
    extern void fill_DMA1_SPI1TxBuffer(void);
    //    LL_GPIO_WriteOutputPort(GPIOF,
    //    LL_GPIO_ReadOutputPort(GPIOF) | LL_GPIO_PIN_1);
    fill_DMA1_SPI1TxBuffer();
    //    LL_GPIO_WriteOutputPort(GPIOF,
    //    LL_GPIO_ReadOutputPort(GPIOF) & ~LL_GPIO_PIN_1);
}

#ifndef SPI_DMA_ENB
/**
 * When enabled this is the handler for the SPI interrupts.
 */
void SPI1_IRQHandler(void)
{
    if(LL_SPI_IsActiveFlag_RXNE(SPI1)) {
        SpiRxAndAct();
    } else if(LL_SPI_IsActiveFlag_TXE(SPI1)) {
        SpiTxMt();
    } else if(LL_SPI_IsActiveFlag_OVR(SPI1)) {
        SpiErrFlag();
    }
}
#endif

/** USART */
/**
 * @brief  This function handles DMAC1 - Chan 4 & 5 interrupt request.
 * @param  None
 * @retval None
 */
#ifdef USE_USART
void DMA1_Channel4_5_IRQHandler(void)
{
    if(LL_DMA_IsActiveFlag_TC4(DMA1)) {
        LL_DMA_ClearFlag_GI4(DMA1);
        /* Call function Transmission complete Callback */
        DMA1_TransmitComplete_Callback();
    } else if(LL_DMA_IsActiveFlag_TE4(DMA1)) {
        /* Call Error function */
        USART_TransferError_Callback();
    }

    if(LL_DMA_IsActiveFlag_TC5(DMA1)) {
        LL_DMA_ClearFlag_GI5(DMA1);
        /* Call function Reception complete Callback */
        DMA1_ReceiveComplete_Callback();
    } else if(LL_DMA_IsActiveFlag_TE5(DMA1)) {
        /* Call Error function */
        USART_TransferError_Callback();
    }
}
#endif

/**
 * this used to trigger the adc conversion in a deterministic
 * time-frame
 */
void TIM3_IRQHandler(void)
{
    if(LL_TIM_IsActiveFlag_UPDATE(TIM3) == 1) {
        //LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_1);
        LL_TIM_ClearFlag_UPDATE(TIM3);

        if(LL_ADC_IsEnabled(ADC1) && !LL_ADC_REG_IsConversionOngoing(ADC1))
            LL_ADC_REG_StartConversion(ADC1);
        //LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_1);
    }
}

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
