/**
 * \file
 * \brief ADC setup and control taken from:
 *      Examples_LL/ADC/ADC_MultiChannelSingleConversion/Src/main.c
 */
#include "main.h"

/**
 * Timeout values for ADC operations.
 * (calibration, enable settling time, disable settling time, ...)
 * Values defined to be higher than worst cases: low clock frequency,
 * maximum prescalers.
* Example of profile very low frequency : ADC clock frequency 0.14MHz
* prescaler 256, sampling time 160.5 ADC clock cycles, resolution 12 bits.
*  - ADC calibration time: On STM32L0 ADC, maximum delay is 83/fADC,
*    resulting in a maximum delay of 593us
*    (refer to device datasheet, parameter "tCAL")
*  - ADC enable time: maximum delay is 1 conversion cycle.
*    (refer to device datasheet, parameter "tSTAB")
*  - ADC disable time: maximum delay should be a few ADC clock cycles
*  - ADC stop conversion time: maximum delay should be a few ADC clock
*    cycles
*  - ADC conversion time: with this hypothesis of clock settings, maximum
*    delay will be 293ms.
*    (refer to device reference manual, section "Timing")
* Unit: ms
*/ 
#define ADC_CALIBRATION_TIMEOUT_MS      ((uint32_t)   1)
#define ADC_ENABLE_TIMEOUT_MS           ((uint32_t)   1)
#define ADC_DISABLE_TIMEOUT_MS          ((uint32_t)   1)
#define ADC_STOP_CONVERSION_TIMEOUT_MS  ((uint32_t)   1)
#define ADC_CONVERSION_TIMEOUT_MS       ((uint32_t) 300)

/**
 * Delay between ADC end of calibration and ADC enable.
 * Delay estimation in CPU cycles: Case of ADC enable done 
 * immediately after ADC calibration, ADC clock setting slow
 * (LL_ADC_CLOCK_ASYNC_DIV32). Use a higher delay if ratio
 * (CPU clock / ADC clock) is above 32.
 */
#define ADC_DELAY_CALIB_ENABLE_CPU_CYCLES       \
    (LL_ADC_DELAY_CALIB_ENABLE_ADC_CYCLES * 32)

/**
 * Definitions of data related to this example
 * Definition of ADCx conversions data table size
 * Size of array set to ADC sequencer number of ranks converted,
 * to have a rank in each array address.
 */
#define ADC_CONVERTED_DATA_BUFFER_SIZE  ((uint32_t) 3)
/** Variables for ADC conversion data
 * ADC group regular conversion data
 *
 * this will contain conversions */
__IO uint16_t aADCxConvertedData[ADC_CONVERTED_DATA_BUFFER_SIZE];

/** Variable to report status of ADC group regular sequence conversions:
 *  0: ADC group regular sequence conversions are not completed
 *  1: ADC group regular sequence conversions are completed
 */
__IO uint8_t ubAdcGrpRegularSequenceConvStatus = 0; /**< Variable set into
                                                     * ADC interrupt
                                                     * callback */
/** Variable to report status of DMA transfer of ADC group regular conversions
 *  0: DMA transfer is not completed
 *  1: DMA transfer is completed
 *  2: DMA transfer has not been started yet (initial state)
 */
__IO uint8_t ubDmaTransferStatus = 0;
__IO uint32_t ubAdcGrpRegularSequenceConvCount = 0;

/* Definitions of environment analog values
 * Value of analog reference voltage (Vref+), connected to analog voltage
 * supply Vdda (unit: mV).  */
#define VDDA_APPLI              ((uint32_t)3300)

/* Init variable out of expected ADC conversion data range */
#define VAR_CONVERTED_DATA_INIT_VALUE   \
    (__LL_ADC_DIGITAL_SCALE(LL_ADC_RESOLUTION_12B) + 1)

/* Parameters of timer (used as ADC conversion trigger)
 * Timer frequency (unit: Hz). With a timer 16 bits and time base
 * freq min 1Hz, range is min=1Hz, max=32kHz. */
#define TIMER_FREQUENCY         ((uint32_t) 2000)
/* Timer minimum frequency (unit: Hz), used to calculate frequency range.
 * With a timer 16 bits, maximum frequency will be 32000 times this value. */
#define TIMER_FREQUENCY_RANGE_MIN      ((uint32_t)    1)
/* Timer prescaler maximum value (0xFFFF for a timer 16 bits) */
#define TIMER_PRESCALER_MAX_VALUE      ((uint32_t)0xFFFF-1)

/**
 * Four interesting ADCs, for now.  Channel1, Channel2, Temperature,
 *    and the VREF voltage.
 */
static struct adc_s_val ch0_adc;
static struct adc_s_val ch1_adc;
static struct adc_s_val temp_adc;
//static struct adc_s_val vref_adc;

/**
  * @brief  This function configures DMA for transfer of data from ADC
  * @param  None
  * @retval None
  */
void Configure_DMA(void)
{
    /*## Configuration of NVIC ##############################################*/
    /* Configure NVIC to enable DMA interruptions */
    NVIC_SetPriority(DMA1_Channel1_IRQn, 1);  /* DMA IRQ lower priority
                                                 than ADC IRQ */
    NVIC_EnableIRQ(DMA1_Channel1_IRQn);

    /*## Configuration of DMA ###############################################*/
    /* Enable the peripheral clock of DMA */
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);

    /*
     * Configure the DMA transfer
     *  - DMA transfer in circular mode to match with ADC configuration:
     *    DMA unlimited requests.
     *  - DMA transfer from ADC without address increment.
     *  - DMA transfer to memory with address increment.
     *  - DMA transfer from ADC by half-word to match with ADC configuration:
     *    ADC resolution 12 bits.
     *  - DMA transfer to memory by half-word to match with
     *        ADC conversion data
     *    buffer variable type: half-word.
     */
    LL_DMA_ConfigTransfer(DMA1,
                          LL_DMA_CHANNEL_1,
                          LL_DMA_DIRECTION_PERIPH_TO_MEMORY |
                          LL_DMA_MODE_CIRCULAR              |
                          LL_DMA_PERIPH_NOINCREMENT         |
                          LL_DMA_MEMORY_INCREMENT           |
                          LL_DMA_PDATAALIGN_HALFWORD        |
                          LL_DMA_MDATAALIGN_HALFWORD        |
                          LL_DMA_PRIORITY_HIGH);

    /* Set DMA transfer addresses of source and destination */
    LL_DMA_ConfigAddresses(DMA1,
                           LL_DMA_CHANNEL_1,
                           LL_ADC_DMA_GetRegAddr(ADC1,
                                                 LL_ADC_DMA_REG_REGULAR_DATA),
                           (uint32_t)&aADCxConvertedData,
                           LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

    /* Set DMA transfer size */
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_1,
                         ADC_CONVERTED_DATA_BUFFER_SIZE);

    /* Enable DMA transfer interruption: transfer complete */
    LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_1);

    /* Enable DMA transfer interruption: transfer error */
    LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_1);

    /*## Activation of DMA ##################################################*/
    /* Enable the DMA transfer */
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);
}

/**
  * @brief  Configure ADC (ADC instance: ADC1) and GPIO used by ADC channels.
  * @note   In case re-use of this function outside of this example:
  *         This function includes checks of ADC hardware constraints before
  *         executing some configuration functions.
  *         - In this example, all these checks are not necessary but are
  *           implemented anyway to show the best practice usages
  *           corresponding to reference manual procedure.
  *           (On some STM32 series, setting of ADC features are not
  *           conditioned to ADC state. However, in order to be compliant with
  *           other STM32 series and to show the best practice usages,
  *           ADC state is checked anyway with same constraints).
  *           Software can be optimized by removing some of these checks,
  *           if they are not relevant considering previous settings and actions
  *           in user application.
  *         - If ADC is not in the appropriate state to modify some parameters,
  *           the setting of these parameters is bypassed without error
  *           reporting:
  *           it can be the expected behavior in case of recall of this
  *           function to update only a few parameters (which update fullfills
  *           the ADC state).
  *           Otherwise, it is up to the user to set the appropriate error
  *           reporting in user application.
  * @note   Peripheral configuration is minimal configuration from reset values.
  *         Thus, some useless LL unitary functions calls below are provided as
  *         commented examples - setting is default configuration from reset.
  * @param  None
  * @retval None
  */
void Configure_ADC(void)
{
    __IO uint32_t wait_loop_index = 0;

    /*## Configuration of GPIO used by ADC channels #########################*/

    /* Enable GPIO Clock */
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);

    /* Configure GPIO PA0 in analog mode to be used as ADC input - Chan 0 */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_0, LL_GPIO_MODE_ANALOG);

    /* Configure GPIO PA1 in analog mode to be used as ADC input - Chan 1 */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_1, LL_GPIO_MODE_ANALOG);

    /*## Configuration of NVIC #############################################*/
    /* Configure NVIC to enable ADC1 interruptions */
    NVIC_SetPriority(ADC1_COMP_IRQn, 0); /* ADC IRQ greater priority
                                            than DMA IRQ */
    NVIC_EnableIRQ(ADC1_COMP_IRQn);

    /*## Configuration of ADC ###############################################*/

    /*## Configuration of ADC hierarchical scope: common to several ADC #####*/

    /* Enable ADC clock (core clock) on PBus */
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_ADC1);

    /*
     * Note: Hardware constraint (refer to description of the functions below):
     *       On this STM32 series, setting of these features is conditioned
     *       to ADC state:
     *       All ADC instances of the ADC common group must be disabled.
     * Note: In this example, all these checks are not necessary but are
     *       implemented anyway to show the best practice usages
     *       corresponding to reference manual procedure.
     *       Software can be optimized by removing some of these checks, if
     *       they are not relevant considering previous settings and actions
     *       in user application.
     */
    if(__LL_ADC_IS_ENABLED_ALL_COMMON_INSTANCE() == 0) {
        /*
         * Note: Call of the functions below are commented because they are
         *       useless in this example:
         *           setting corresponding to default configuration from
         *           reset state.
         *
         * Set ADC clock (conversion clock) common to several ADC instances
         * Note: On this STM32 serie, ADC common clock asynchonous prescaler
         *       is applied to each ADC instance if ADC instance clock is
         *       set to clock source asynchronous
         *       (refer to function "LL_ADC_SetClock()" below).
         */
        // LL_ADC_SetCommonClock(__LL_ADC_COMMON_INSTANCE(ADC1),
        //                       LL_ADC_CLOCK_ASYNC_DIV1);

        /* Set ADC measurement path to internal channels */
        LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(ADC1),
                                       //LL_ADC_PATH_INTERNAL_VREFINT
                                       //| LL_ADC_PATH_INTERNAL_TEMPSENSOR
                                       LL_ADC_PATH_INTERNAL_TEMPSENSOR
                                       );

        /*
         * Delay for ADC temperature sensor stabilization time.
         * Compute number of CPU cycles to wait for, from delay in us.
         * Note: Variable divided by 2 to compensate partially
         *       CPU processing cycles (depends on compilation optimization).
         * Note: If system core clock frequency is below 200kHz, wait time
         *       is only a few CPU processing cycles.
         * Note: This delay is implemented here for the purpose in
         *       this example.
         *       It can be optimized if merged with other delays
         *       during ADC activation or if other actions are performed
         *       in the meantime.
         */
        wait_loop_index = ((LL_ADC_DELAY_TEMPSENSOR_STAB_US
                            * (SystemCoreClock / (100000 * 2))) / 10);
        while(wait_loop_index != 0) {
            wait_loop_index--;
        }
    }


    /*## Configuration of ADC hierarchical scope: ADC instance ##############*/

    /*
     * Note: Hardware constraint (refer to description of the functions below):
     *       On this STM32 serie, setting of these features is conditioned to
     *       ADC state:
     *       ADC must be disabled.
     */
    if(LL_ADC_IsEnabled(ADC1) == 0) {
        /*
         * Note: Call of the functions below are commented because they are
         *       useless in this example:
         *       setting corresponding to default configuration from
         *       reset state.
         */

        /* Set ADC clock (conversion clock) */
        LL_ADC_SetClock(ADC1, LL_ADC_CLOCK_SYNC_PCLK_DIV2);

        /* Set ADC data resolution */
        LL_ADC_SetResolution(ADC1, LL_ADC_RESOLUTION_12B);

        /* Set ADC conversion data alignment */
        LL_ADC_SetDataAlignment(ADC1, LL_ADC_DATA_ALIGN_RIGHT);

        /* Set ADC low power mode */
        // LL_ADC_SetLowPowerMode(ADC1, LL_ADC_LP_MODE_NONE);

        /*
         * Set ADC channels sampling time
         * Note: On this STM32 serie, sampling time is common to all channels
         *       of the entire ADC instance.
         *       Therefore, sampling time is configured here under
         *       ADC instance
         *       scope (not under channel scope as on some other STM32 devices
         *       on which sampling time is channel wise).
         * Note: Considering interruption occurring after each ADC group
         *       regular sequence conversions
         *       (IT from DMA transfer complete),
         *       select sampling time and ADC clock with sufficient
         *       duration to not create an overhead situation in IRQHandler.
         */
        LL_ADC_SetSamplingTimeCommonChannels(ADC1,
                                             LL_ADC_SAMPLINGTIME_28CYCLES_5);

    }


    /*## Configuration of ADC hierarchical scope: ADC group regular ##########*/
    /*
     * Note: Hardware constraint (refer to description of the functions below):
     *       On this STM32 serie, setting of these features is conditioned to
     *       ADC state:
     *       ADC must be disabled or enabled without conversion on going
     *       on group regular.
     */
    if((LL_ADC_IsEnabled(ADC1) == 0)
       || (LL_ADC_REG_IsConversionOngoing(ADC1) == 0)) {
        /* Set ADC group regular trigger source */
        /* LL_ADC_REG_SetTriggerSource(ADC1, LL_ADC_REG_TRIG_SOFTWARE); */
        LL_ADC_REG_SetTriggerSource(ADC1, LL_ADC_REG_TRIG_SOFTWARE);

        /* Set ADC group regular trigger polarity */
        // LL_ADC_REG_SetTriggerEdge(ADC1, LL_ADC_REG_TRIG_EXT_RISING);

        /* Set ADC group regular continuous mode */
        LL_ADC_REG_SetContinuousMode(ADC1, LL_ADC_REG_CONV_CONTINUOUS);

        /* Set ADC group regular conversion data transfer */
        LL_ADC_REG_SetDMATransfer(ADC1, LL_ADC_REG_DMA_TRANSFER_LIMITED);

        /* Set ADC group regular overrun behavior */
        LL_ADC_REG_SetOverrun(ADC1, LL_ADC_REG_OVR_DATA_OVERWRITTEN);

        /* Set ADC group regular sequencer */
        /* Note: On this STM32 serie, ADC group regular sequencer is
         *       not fully configurable: sequencer length and each rank
         *       affectation to a channel are fixed by channel HW number.
         *       Refer to description of function
         *       "LL_ADC_REG_SetSequencerChannels()".
         *       Case of STM32F0xx:
         *       ADC Channel ADC_CHANNEL_TEMPSENSOR is on ADC channel 16,
         *       there is 1 other channel enabled with lower channel number.
         *       Therefore, ADC_CHANNEL_TEMPSENSOR will be converted by the
         *       sequencer as the 2nd rank.
         *       ADC Channel ADC_CHANNEL_VREFINT is on ADC channel 17,
         *       there are 2 other channels enabled with lower channel number.
         *       Therefore, ADC_CHANNEL_VREFINT will be converted by the
         *       sequencer as the 3rd rank.
         * Set ADC group regular sequencer discontinuous mode */
        // LL_ADC_REG_SetSequencerDiscont(ADC1, LL_ADC_REG_SEQ_DISCONT_DISABLE);

        /* Set ADC group regular sequence: channel on rank corresponding to
         * channel number. */
        LL_ADC_REG_SetSequencerChannels(ADC1,
                                        LL_ADC_CHANNEL_0
                                        | LL_ADC_CHANNEL_1
                                        | LL_ADC_CHANNEL_VREFINT
                                        | LL_ADC_CHANNEL_TEMPSENSOR);
    }

    /*## Configuration of ADC hierarchical scope: ADC group injected #########*/
    /* Note: Feature not available on this STM32 serie */

    /*## Configuration of ADC hierarchical scope: channels ###################*/

    /*
     * Note: Hardware constraint (refer to description of the functions below):
     *       On this STM32 serie, setting of these features is conditioned to
     *       ADC state:
     *       ADC must be disabled or enabled without conversion on going
     *       on either groups regular or injected.
     */
    if((LL_ADC_IsEnabled(ADC1) == 0) ||
       (LL_ADC_REG_IsConversionOngoing(ADC1) == 0)) {
        /* Set ADC channels sampling time
         * Note: On this STM32 serie, sampling time is common to all channels
         *       of the entire ADC instance.
         *       See sampling time configured above, at ADC instance scope.
         */
    }


    /*## Configuration of ADC interruptions ##################################*/
    /* Enable interruption ADC group regular end of sequence conversions */
    LL_ADC_EnableIT_EOS(ADC1);

    /* Enable interruption ADC group regular overrun */
    LL_ADC_EnableIT_OVR(ADC1);

    /* Note: in this example, ADC group regular end of conversions
     *       (number of ADC conversions defined by DMA buffer size)
     *       are notified by DMA transfer interruptions).
     *       ADC interruptions of end of conversion are enabled optionally,
     *       as demonstration purpose in this example. */
}

/**
  * @brief  Configure timer as a time base (timer instance: TIM3) 
  *         used to trig ADC conversion start.
  * @note   In this ADC example, timer instance must be on APB1 (clocked by PCLK1)
  *         to be compliant with frequency calculation used in this function.
  * @param  None
  * @retval None
  */
void Configure_TIM_TimeBase_ADC_trigger(void)
{
    uint32_t timer_clock_frequency = 0; /* Timer clock frequency */
    uint32_t timer_prescaler = 0;       /* Time base prescaler to have
                                           timebase aligned on minimum
                                           frequency possible */
    uint32_t timer_reload = 0;          /* Timer reload value in function
                                           of timer prescaler to achieve
                                           time base period */

    /*## Configuration of NVIC ############*/
    /*
     * If needed, timer interruption at each time base period is
     * possible and in use.
     *
     * Configuration of timer as time base: 
     * Caution: Computation of frequency is done for a timer instance on APB1
     *          (clocked by PCLK1)
     * Timer frequency is configured from the following constants:
     * - TIMER_FREQUENCY: timer frequency (unit: Hz).
     * - TIMER_FREQUENCY_RANGE_MIN: timer minimum frequency possible (unit: Hz).
     * Note: Refer to comments at these literals definition for more details.
     *
     * Retrieve timer clock source frequency
     * If APB1 prescaler is different of 1, timers have a factor x2 on their
     * clock source. */
    if (LL_RCC_GetAPB1Prescaler() == LL_RCC_APB1_DIV_1) {
        timer_clock_frequency = SystemCoreClock;
    } else {
        timer_clock_frequency = (SystemCoreClock * 2);
    }

    /* Timer prescaler calculation
     * (computation for timer 16 bits, additional + 1 to round
     *  the prescaler up) */
    timer_prescaler = ((timer_clock_frequency / (TIMER_PRESCALER_MAX_VALUE
                                                 * TIMER_FREQUENCY_RANGE_MIN))
                       + 1);
    /* Timer reload calculation */
    timer_reload = timer_clock_frequency / (timer_prescaler * TIMER_FREQUENCY);

    /* Enable the timer peripheral clock */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);

    /* Set timer pre-scaler value */
    LL_TIM_SetPrescaler(TIM3, timer_prescaler - 1);

    /* Set timer auto-reload value */
    LL_TIM_SetAutoReload(TIM3, timer_reload - 1);

    /* Counter mode: select up-counting mode */
    LL_TIM_SetCounterMode(TIM3, LL_TIM_COUNTERMODE_UP); 

    LL_TIM_EnableARRPreload(TIM3);      /* enable auto reload */
    LL_TIM_EnableIT_UPDATE(TIM3);

    NVIC_SetPriority(TIM3_IRQn, 0);
    NVIC_EnableIRQ(TIM3_IRQn);

    /* Enable counter */
    LL_TIM_EnableCounter(TIM3);

    LL_TIM_GenerateEvent_UPDATE(TIM3);
}

/**
  * @brief  Perform ADC activation procedure to make it ready to convert
  *         (ADC instance: ADC1).
  * @note   Operations:
  *         - ADC instance
  *           - Run ADC self calibration
  *           - Enable ADC
  *         - ADC group regular
  *           none: ADC conversion start-stop to be performed
  *                 after this function
  *         - ADC group injected
  *           Feature not available
  *           (feature not available on this STM32 series)
  * @param  None
  * @retval None
  */
void Activate_ADC(void)
{
    __IO uint32_t wait_loop_index = 0;
    __IO uint32_t backup_setting_adc_dma_transfer = 0;
#if (USE_TIMEOUT == 1)
    uint32_t Timeout = 0; /* Variable used for timeout management */
#endif /* USE_TIMEOUT */

    /*## Operation on ADC hierarchical scope: ADC instance ###################*/
    /*
     * Note: Hardware constraint (refer to description of the functions below):
     *       On this STM32 serie, setting of these features is conditioned to
     *       ADC state:
     *       ADC must be disabled.
     * Note: In this example, all these checks are not necessary but are
     *       implemented anyway to show the best practice usages
     *       corresponding to reference manual procedure.
     *       Software can be optimized by removing some of these checks, if
     *       they are not relevant considering previous settings and actions
     *       in user application.
     */
    if(LL_ADC_IsEnabled(ADC1) == 0) {
        /* Disable ADC DMA transfer request during calibration
         * Note: Specificity of this STM32 serie: Calibration factor is
         *       available in data register and also transfered by DMA.
         *       To not insert ADC calibration factor among ADC conversion
         *       data in array variable, DMA transfer must be disabled during
         *       calibration. */
        backup_setting_adc_dma_transfer = LL_ADC_REG_GetDMATransfer(ADC1);
        LL_ADC_REG_SetDMATransfer(ADC1, LL_ADC_REG_DMA_TRANSFER_NONE);

        /* Run ADC self calibration */
        LL_ADC_StartCalibration(ADC1);

        /* Poll for ADC effectively calibrated */
#if (USE_TIMEOUT == 1)
        Timeout = ADC_CALIBRATION_TIMEOUT_MS;
#endif /* USE_TIMEOUT */

        while(LL_ADC_IsCalibrationOnGoing(ADC1) != 0) {
#if (USE_TIMEOUT == 1)
            /* Check Systick counter flag to decrement the time-out value */
            if(LL_SYSTICK_IsActiveCounterFlag()) {
                if(Timeout-- == 0) {
                    /* Time-out occurred. Set LED to blinking mode */
                    LED_Blinking(LED_BLINK_ERROR);
                }
            }
#endif /* USE_TIMEOUT */
        }

        /* Delay between ADC end of calibration and ADC enable.
         * Note: Variable divided by 2 to compensate partially
         * CPU processing cycles (depends on compilation optimization).*/
        wait_loop_index = (ADC_DELAY_CALIB_ENABLE_CPU_CYCLES >> 1);
        while(wait_loop_index != 0) {
            wait_loop_index--;
        }

        /* Restore ADC DMA transfer request after calibration */
        LL_ADC_REG_SetDMATransfer(ADC1, backup_setting_adc_dma_transfer);

        /* Enable ADC */
        LL_ADC_Enable(ADC1);

        /* Poll for ADC ready to convert */
#if (USE_TIMEOUT == 1)
        Timeout = ADC_ENABLE_TIMEOUT_MS;
#endif /* USE_TIMEOUT */

        while(LL_ADC_IsActiveFlag_ADRDY(ADC1) == 0) { }

        /* Note: ADC flag ADRDY is not cleared here to be able to check ADC
         * status afterwards.
         * This flag should be cleared at ADC Deactivation, before a new
         * ADC activation, using function "LL_ADC_ClearFlag_ADRDY()". */
    }
}

/**
  * @brief  DMA transfer error callback
  * @note   This function is executed when the transfer error interrupt
  *         is generated during DMA transfer
  * @retval None
  */
void AdcDmaTransferError_Callback(void)
{
    /* Error detected during DMA transfer
     * this should be logged and made accessible to someone/somehow */
}

static void fill_vals(struct adc_s_val *ch, uint16_t val)
{
#if 0
    ch->total = val;
#else
    /*  enable this path if and when oversampling becomes necessary */
    ch->total -= ch->vals[ch->idx];
    ch->total += val;
    ch->vals[ch->idx] = val;
    ch->idx = (ch->idx + 1) & (OVRSAMPLE_LEN - 1);
#endif
}

static void fill_temp_vals(struct adc_s_val *ch, uint16_t val)
{
#   define TEMP110_CAL_ADDR     ((uint16_t*) 0x1ffff7c2)
#   define TEMP30_CAL_ADDR      ((uint16_t*) 0x1ffff7b8)
    int32_t temp;

    temp = val - *(uint16_t*) TEMP30_CAL_ADDR;
    temp *= 80;
    temp /= *(uint16_t*) TEMP110_CAL_ADDR - *(uint16_t*) TEMP30_CAL_ADDR;
    val = temp + 30;

    /*  enable this path if and when oversampling becomes necessary */
    ch->total -= ch->vals[ch->idx];
    ch->total += val;
    ch->vals[ch->idx] = val;
    ch->idx = (ch->idx + 1) & (OVRSAMPLE_LEN - 1);
}

/**
  * \brief  DMA transfer complete callback
  * \note   This function is executed when the transfer complete interrupt
  *         is generated
  * \retval None
  */
void AdcDmaTransferComplete_Callback(void)
{
    uint32_t i;
    /* Update status variable of DMA transfer */
    ubDmaTransferStatus = 1;

    /* For this example purpose, check that DMA transfer status is matching
     * ADC group regular sequence status: */
    if(ubAdcGrpRegularSequenceConvStatus != 1) {
        AdcDmaTransferError_Callback();
    }

    /* Reset status variable of ADC group regular sequence */
    ubAdcGrpRegularSequenceConvStatus = 0;

    for(i = 0; i < ADC_CONVERTED_DATA_BUFFER_SIZE; ++i) {
        fill_vals(&ch0_adc, aADCxConvertedData[i]);
        fill_vals(&ch1_adc, aADCxConvertedData[i + 1]);
        fill_temp_vals(&temp_adc, aADCxConvertedData[i + 2]);
        //fill_vals(&vref_adc, aADCxConvertedData[i + 3]);
        i += 3;
    }
}

/**
  * @brief  ADC group regular end of sequence conversions interruption callback
  * @note   This function is executed when the ADC group regular
  *         sequencer has converted all ranks of the sequence.
  * @retval None
  */
void AdcGrpRegularSequenceConvComplete_Callback(void)
{
    /* Update status variable of ADC group regular sequence */
    ubAdcGrpRegularSequenceConvStatus = 1;
    ubAdcGrpRegularSequenceConvCount++;
}

/**
  * @brief  ADC group regular overrun interruption callback
  * @note   This function is executed when ADC group regular
  *         overrun error occurs.
  * @retval None
  */
void AdcGrpRegularOverrunError_Callback(void)
{
    /* Note: Disable ADC interruption that caused this error before
     *       clearing the buffers and restarting. */

    /* Disable ADC group regular overrun interruption */
    LL_ADC_DisableIT_OVR(ADC1);
}

uint16_t ch0_total(void)
{
    return ch0_adc.total / OVRSAMPLE_LEN;
}

uint16_t ch1_total(void)
{
    return ch1_adc.total / OVRSAMPLE_LEN;
}

uint16_t temp_total(void)
{
    return temp_adc.total / OVRSAMPLE_LEN;
}

/* uint16_t vref_total(void) */
/* { */
/*     return vref_adc.total; */
/* } */

/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
