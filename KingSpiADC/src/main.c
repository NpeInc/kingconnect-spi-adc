/**
  ******************************************************************************
  * @file
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    27-May-2016
  * @brief   This example describes how to use a ADC peripheral to convert
  *          several channels, ADC conversions are performed successively
  *          in a scan sequence.
  *          This example is based on the STM32F0xx ADC LL API;
  *          Peripheral initialization done using LL unitary services functions.
  *          from: Examples_LL/ADC/ADC_MultiChannelSingleConversion/Src/main.c
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/** @addtogroup STM32F0xx_LL_Examples
  * @{
  */

/** @addtogroup ADC_MultiChannelSingleConversion
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#define NOP     __asm__("nop")

/** Variable to report status of DMA transfer of ADC group regular conversions
 *  0: DMA transfer is not completed
 *  1: DMA transfer is completed
 *  2: DMA transfer has not been started yet (initial state)
 */
extern __IO uint8_t ubDmaTransferStatus; /**< Variable set into DMA
                                          * interruption callback */
extern volatile uint8_t ubDmaDisabledError;
extern char *buildtimeanddate;

/* Private function prototypes -----------------------------------------------*/
void     SystemClock_Config(void);
uint32_t RCC_StartHSIAndWaitForHSIReady();

/* Private functions ---------------------------------------------------------*/

void gpio_enb(void)
{
    /* enable the GPIO ports we will be using */
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOF);

    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_2, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_2, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_2, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_2, LL_GPIO_PULL_DOWN);

    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_3, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_3, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_3, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_3, LL_GPIO_PULL_DOWN);
#ifndef USE_USART
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_9, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_9, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_9, LL_GPIO_PULL_DOWN);

    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_10, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_10, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_10, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_10, LL_GPIO_PULL_DOWN);
#endif
    /* enable PB1 as a GPIO output for timing purposes */
    LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_1, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOB, LL_GPIO_PIN_1, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinSpeed(GPIOB, LL_GPIO_PIN_1, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull(GPIOB, LL_GPIO_PIN_1, LL_GPIO_PULL_DOWN);

    /* two more PF0 and PF1 */
    LL_GPIO_SetPinMode(GPIOF, LL_GPIO_PIN_0, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOF, LL_GPIO_PIN_0, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinSpeed(GPIOF, LL_GPIO_PIN_0, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull(GPIOF, LL_GPIO_PIN_0, LL_GPIO_PULL_DOWN);

    LL_GPIO_SetPinMode(GPIOF, LL_GPIO_PIN_1, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(GPIOF, LL_GPIO_PIN_1, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinSpeed(GPIOF, LL_GPIO_PIN_1, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull(GPIOF, LL_GPIO_PIN_1, LL_GPIO_PULL_DOWN);
}

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
//    extern void do_builddate(void);
//    volatile char *x;
    /* Configure the system clock to 14 MHz */
    SystemClock_Config();
#if 1
    FLASH_Unlock();
    FLASH_OB_Unlock();
    FLASH_OB_BOOTConfig(OB_BOOT1_SET);
    FLASH_WaitForLastOperation(FLASH_ER_PRG_TIMEOUT);
    FLASH_OB_Lock();
    FLASH_Lock();
#endif
//    do_builddate();
//    x = buildtimeanddate;
    /* set up the GPIO pins */
    gpio_enb();

    if(RCC_StartHSIAndWaitForHSIReady() != 0) { }

    /* Configure ADC
     * Note: This function configures the ADC but does not enable it.
     * To enable it, use function "Activate_ADC()".
     * This is intended to optimize power consumption:
     * 1. ADC configuration can be done once at the beginning
     *    (ADC disabled, minimal power consumption)
     * 2. ADC enable (higher power consumption) can be done just before
     *    ADC conversions needed.
     * Then, possible to perform successive "Activate_ADC()",
     *    "Deactivate_ADC()", ..., without having to set again
     * ADC configuration.
     */
    Configure_ADC();

    /* Configure DMA for data transfer from ADC */
    Configure_DMA();

    Configure_TIM_TimeBase_ADC_trigger();

    Configure_SPI();
#ifdef SPI_DMA_ENB
    Configure_SPI_DMA();
#endif
    /* use the USART to send current version/revision information */
    Configure_USART();
#ifdef USE_USART
    Configure_USART_DMA();
#endif
    /* Activate ADC
     * Perform ADC activation procedure to make it ready to convert. */
    Activate_ADC();
    /* Activate SPI
     * Perform SPI activation procedure to transfer data. */
    Activate_SPI();

    /* Infinite loop */
    while(1) {
        /* Note: For this example purposes, number of ADC group regular
         *       sequences completed are stored into variable
         *       "ubAdcGrpRegularSequenceConvCount"
         *       (for debug: see variable content into watch window) */

        /* Note: ADC conversions data are stored into array aADCxConvertedData
         *       (for debug: see variable content into watch window).
         *       Each rank of the sequence is at an address of the array:
         *       - aADCxConvertedData[0]: ADC channel set on rank1
         *                                (ADC1 channel 4)
         *       - aADCxConvertedData[1]: ADC channel set on rank2
         *                                (ADC1 internal channel VrefInt)
         *       - aADCxConvertedData[2]: ADC channel set on rank3
         *                                (ADC1 internal channel temperature
         *                                 sensor) */

        /* Wait for ADC conversion and DMA transfer completion to process data*/
        while(ubDmaTransferStatus != 1) {
            if(ubDmaDisabledError) {
                LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_2);
                LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_3);
                ubDmaDisabledError = 0;
            }
        }
#if 0
        {
            volatile uint16_t tempsensor_cal1, tempsensor_cal2;
            volatile uint16_t tempsensor_cal1temp, tempsensor_cal2temp;

            tempsensor_cal1 = *(uint16_t*) TEMPSENSOR_CAL1_ADDR;
            tempsensor_cal2 = *(uint16_t*) TEMPSENSOR_CAL2_ADDR;
            tempsensor_cal1temp = *(uint16_t*) TEMPSENSOR_CAL1_TEMP;
            tempsensor_cal2temp = *(uint16_t*) TEMPSENSOR_CAL2_TEMP;
            __asm__("nop");
        }
#endif
        /* Wait for a new ADC conversion and DMA transfer */
        while(ubDmaTransferStatus != 0) {
            if(ubDmaDisabledError) {
                LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_2);
                LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_3);
                ubDmaDisabledError = 0;
            }
        }
    }
}

/**
  * @brief  Enable HSI and Wait for HSI ready
  * @param  None
  * @retval RCC_ERROR_NONE if no error
  */
uint32_t RCC_StartHSIAndWaitForHSIReady()
{
    /* Enable HSI and wait for HSI ready*/
    LL_RCC_HSI_Enable();
  
#if (USE_TIMEOUT == 1)
    Timeout = HSI_TIMEOUT_VALUE;
#endif /* USE_TIMEOUT */
    while(LL_RCC_HSI_IsReady() != 1) {
#if (USE_TIMEOUT == 1)
        /* Check Systick counter flag to decrement the Time-out value */
        if(LL_SYSTICK_IsActiveCounterFlag()) { 
            if(Timeout-- == 0) {
                /* Time-out occurred. Return an error */
                return 110;
            }
        } 
#endif /* USE_TIMEOUT */
    };

    return 0;
}

/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow : 
 *            System Clock source            = PLL (HSI48)
 *            SYSCLK(Hz)                     = 48000000
 *            HCLK(Hz)                       = 48000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 1
 *            HSI Frequency(Hz)              = 48000000
 *            PREDIV                         = 2
 *            PLLMUL                         = 2
 *            Flash Latency(WS)              = 1
 * @param  None
 * @retval None
 */
void SystemClock_Config(void)
{
    /* Set FLASH latency */ 
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_1);

    /* Enable HSI48 and wait for activation*/
    LL_RCC_HSI_Enable(); 
    while(LL_RCC_HSI_IsReady() != 1) { }

    /* Main PLL configuration and activation */
    LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI_DIV_2, LL_RCC_PLL_MUL_12);
  
    LL_RCC_PLL_Enable();
    while(LL_RCC_PLL_IsReady() != 1) { }
  
    /* Sysclk activation on the main PLL */
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
    while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL) { }
  
    /* Set APB1 prescaler */
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  
    /* Set systick to 1ms in using frequency set to 48MHz
     * This frequency can be calculated through LL RCC macro
     * ex: __LL_RCC_CALC_PLLCLK_FREQ (HSI48_VALUE,
     *                                LL_RCC_PLL_MUL_2, LL_RCC_PREDIV_DIV_2) */
    LL_Init1msTick(48000000);
  
    /* Update CMSIS variable (which can be updated also through
       SystemCoreClockUpdate function) */
    LL_SetSystemCoreClock(48000000);
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d", file, line) */

    /* Infinite loop */
    while(1) {
    }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
