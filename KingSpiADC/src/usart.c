/**
  ******************************************************************************
  * @file
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    27-May-2016
  * @brief   This example describes how to send/receive bytes over USART IP using
  *          the STM32F0xx USART LL API in DMA mode.
  *          Peripheral initialization done using LL unitary services functions.
  *          from: Examples_LL/USART/USART_Communication_TxRx_DMA/Src/main.c
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
//#ifdef USE_USART
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/** @addtogroup STM32F0xx_LL_Examples
  * @{
  */

/** @addtogroup USART_Communication_TxRx_DMA
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

__IO uint8_t ubSend = 0;
/* Buffer used for transmission */
//static uint8_t aTxBuffer[] = "STM32F0xx USART LL API Example : TX/RX in DMA mode\r\nConfiguration UART 115200 bps, 8 data bit/1 stop bit/No parity/No HW flow control\r\nPlease enter 'END' string ...\r\n";
//static uint8_t ubNbDataToTransmit = sizeof(aTxBuffer);
static __IO uint8_t ubTransmissionComplete = 0;

/* Buffer used for reception */
const uint8_t aStringToReceive[] = "END";
//static uint8_t ubNbDataToReceive = sizeof(aStringToReceive) - 1;
//static uint8_t aRxBuffer[sizeof(aStringToReceive) - 1];
static __IO uint8_t ubReceptionComplete = 0;

/* Private function prototypes -----------------------------------------------*/
void     StartTransfers(void);

/**
  * @brief  This function configures the DMA Channels for TX and RX transfers
  * @note   This function is used to :
  *         -1- Enable DMA1 clock
  *         -2- Configure NVIC for DMA transfer complete/error interrupts 
  *         -3- Configure DMA TX channel functional parameters
  *         -4- Configure DMA RX channel functional parameters
  *         -5- Enable transfer complete/error interrupts
  * @param  None
  * @retval None
  */
void Configure_USART_DMA(void)
{
#ifdef USE_USART
    /* DMA1 used for USART1 Transmission and Reception
     */
    /* (1) Enable the clock of DMA1 */
    //LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);

    /* (2) Configure NVIC for DMA transfer complete/error interrupts */
    NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn, 0);
    NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);

    /* (3) Configure the DMA functional parameters for transmission */
    LL_DMA_ConfigTransfer(DMA1, LL_DMA_CHANNEL_4, 
                          LL_DMA_DIRECTION_MEMORY_TO_PERIPH | 
                          LL_DMA_PRIORITY_HIGH              | 
                          LL_DMA_MODE_NORMAL                | 
                          LL_DMA_PERIPH_NOINCREMENT         | 
                          LL_DMA_MEMORY_INCREMENT           | 
                          LL_DMA_PDATAALIGN_BYTE            | 
                          LL_DMA_MDATAALIGN_BYTE);
    LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_4,
                           (uint32_t)aTxBuffer,
                           LL_USART_DMA_GetRegAddr(USART1, LL_USART_DMA_REG_DATA_TRANSMIT),
                           LL_DMA_GetDataTransferDirection(DMA1,
                                                           LL_DMA_CHANNEL_4));
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_4, ubNbDataToTransmit);

    /* (4) Configure the DMA functional parameters for reception */
    LL_DMA_ConfigTransfer(DMA1, LL_DMA_CHANNEL_5, 
                          LL_DMA_DIRECTION_PERIPH_TO_MEMORY | 
                          LL_DMA_PRIORITY_HIGH              | 
                          LL_DMA_MODE_NORMAL                | 
                          LL_DMA_PERIPH_NOINCREMENT         | 
                          LL_DMA_MEMORY_INCREMENT           | 
                          LL_DMA_PDATAALIGN_BYTE            | 
                          LL_DMA_MDATAALIGN_BYTE);
    LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_5,
                           LL_USART_DMA_GetRegAddr(USART1, LL_USART_DMA_REG_DATA_RECEIVE),
                           (uint32_t)aRxBuffer,
                           LL_DMA_GetDataTransferDirection(DMA1,
                                                           LL_DMA_CHANNEL_5));
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_5, ubNbDataToReceive);

    /* (5) Enable DMA transfer complete/error interrupts  */
    LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_4);
    LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_4);
    LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_5);
    LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_5);
#endif
}

/**
  * @brief  This function configures USARTx Instance.
  * @note   This function is used to :
  *         -1- Enable GPIO clock and configures the USART1 pins.
  *         -2- Enable the USART1 peripheral clock and clock source.
  *         -3- Configure USART1 functional parameters.
  *         -4- Enable USART1.
  * @note   Peripheral configuration is minimal configuration from reset values.
  *         Thus, some useless LL unitary functions calls below are provided as
  *         commented examples - setting is default configuration from reset.
  * @param  None
  * @retval None
  */
void Configure_USART(void)
{
#ifdef USE_USART
    /* Configure Tx Pin as : Alternate function, High Speed,
       Push pull, Pull up */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_9, LL_GPIO_AF_1);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_9, LL_GPIO_SPEED_FREQ_VERY_HIGH);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_9, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_9, LL_GPIO_PULL_UP);

    /* Configure Rx Pin as : Alternate function, High Speed,
       Push pull, Pull up */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_10, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_10, LL_GPIO_AF_1);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_10, LL_GPIO_SPEED_FREQ_VERY_HIGH);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_10, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_10, LL_GPIO_PULL_UP);

    /* (2) Enable USART1 peripheral clock and clock source ************/
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP2_PERIPH_USART1);

    /* Set clock source */
    LL_RCC_SetUSARTClockSource(LL_RCC_USART1_CLKSOURCE_PCLK1);

    /* (3) Configure USART1 functional parameters **********************/
  
    /* Disable USART prior modifying configuration registers */
    /* Note: Commented as corresponding to Reset value */
    // LL_USART_Disable(USART1);

    /* TX/RX direction */
    LL_USART_SetTransferDirection(USART1, LL_USART_DIRECTION_TX_RX);

    /* 8 data bit, 1 start bit, 1 stop bit, no parity */
    LL_USART_ConfigCharacter(USART1, LL_USART_DATAWIDTH_8B,
                             LL_USART_PARITY_NONE, LL_USART_STOPBITS_1);

    /* No Hardware Flow control */
    /* Reset value is LL_USART_HWCONTROL_NONE */
    LL_USART_SetHWFlowCtrl(USART1, LL_USART_HWCONTROL_NONE);

    /* Oversampling by 16 */
    /* Reset value is LL_USART_OVERSAMPLING_16 */
    LL_USART_SetOverSampling(USART1, LL_USART_OVERSAMPLING_16);

    /* Set Baudrate to 9600 using APB frequency set to 48000000 Hz
     * Frequency available for USART peripheral can also be calculated
     * through LL RCC macro */
    /* Ex :
       Periphclk = LL_RCC_GetUSARTClockFreq(Instance); 
       or LL_RCC_GetUARTClockFreq(Instance);
       depending on USART/UART instance
       In this example, Peripheral Clock is expected to be equal
       to 48000000 Hz => equal to SystemCoreClock */
    LL_USART_SetBaudRate(USART1, SystemCoreClock,
                         LL_USART_OVERSAMPLING_16, 9600); 

    /* (4) Enable USART1 *********************/
    LL_USART_Enable(USART1);
#endif
}

/**
 * @brief  This function initiates TX and RX DMA transfers
 *         by enabling DMA channels
  * @param  None
  * @retval None
  */
void StartTransfers(void)
{
#ifdef USE_USART
    /* Enable DMA RX Interrupt */
    LL_USART_EnableDMAReq_RX(USART1);

    /* Enable DMA TX Interrupt */
    LL_USART_EnableDMAReq_TX(USART1);

    /* Enable DMA Channel Rx */
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);

    /* Enable DMA Channel Tx */
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_4);
#endif
}

/**
  * @brief  Wait end of transfer and check if received Data are well.
  * @param  None 
  * @retval None
  */
void WaitAndCheckEndOfTransfer(void)
{
#ifdef USE_USART
    /* 1 - Wait end of transmission */
    while(ubTransmissionComplete != 1) { }
    /* Disable DMA1 Tx Channel */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_4);

    /* 2 - Wait end of reception */
    while(ubReceptionComplete != 1) { }
    /* Disable DMA1 Rx Channel */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_5);
#endif
}

#ifdef USE_USART
/******************************************************************************/
/*   USER IRQ HANDLER TREATMENT Functions                                     */
/******************************************************************************/

/**
 * @brief  Function called from DMA1 IRQ Handler when Tx transfer is completed 
 * @param  None
 * @retval None
 */
void DMA1_TransmitComplete_Callback(void)
{
    /* DMA Tx transfer completed */
    ubTransmissionComplete = 1;
}

/**
 * @brief  Function called from DMA1 IRQ Handler when Rx transfer is completed 
 * @param  None
 * @retval None
 */
void DMA1_ReceiveComplete_Callback(void)
{
    /* DMA Rx transfer completed */
    ubReceptionComplete = 1;
}

/**
 * @brief  Function called in case of error detected in USART IT Handler
 * @param  None
 * @retval None
 */
void USART_TransferError_Callback(void)
{
    /* Disable DMA1 Tx Channel */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_4);

    /* Disable DMA1 Rx Channel */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_5);
}
#endif

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name
       and line number,
       ex: printf("Wrong parameters value: file %s on line %d", file, line) */
    printf("\r\n!!! ASSERT: %s: %d !!!\r\n", file, line);
    /* Infinite loop */
    while(1) { }
}
#endif

//#endif /* USE_USART */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
