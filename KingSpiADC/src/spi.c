/**
  ******************************************************************************
  * @file
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    27-May-2016
  * @brief   This example describes how to send/receive bytes over SPI IP using
  *          the STM32F0xx SPI LL API.
  *          Peripheral initialization done using LL unitary services functions.
  *          from: Examples_LL/SPI/SPI_TwoBoards_FullDuplex_DMA/Src/main.c
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "spi.h"

/** @addtogroup STM32F0xx_LL_Examples
  * @{
  */

/** @addtogroup SPI_TwoBoards_FullDuplex_DMA
  * @{
  */

/* Private typedef ---------------------------------------------------------*/
/* Private define ----------------------------------------------------------*/
/* Private macro -----------------------------------------------------------*/
/* Uncomment this line to use the board as master, if not it is used as slave */
//#define MASTER_BOARD

/* Private macro -----------------------------------------------------------*/
/* Private variable---------------------------------------------------------*/
volatile uint8_t ubDmaDisabledError = 0;

/** Buffer used for transmission */
#define SPI_RX_BUFFER_LEN  10
#define SPI_TX_BUFFER_LEN  10
static uint8_t SPI_TxBuffer[SPI_TX_BUFFER_LEN];
static uint8_t SPI_NbDataToTransmit = SPI_TX_BUFFER_LEN;

/* Buffer used for reception */
uint8_t SPI_RxBuffer[SPI_RX_BUFFER_LEN];
uint8_t SPI_NbDataToReceive  = SPI_RX_BUFFER_LEN;
__IO uint8_t ubReceptionComplete = 0;

/* Private function prototypes ---------------------------------------------*/

/* Private functions -------------------------------------------------------*/

#ifdef SPI_DMA_ENB
/**
  * @brief This function configures the DMA Channels for SPI1
  * @note  This function is used to :
  *        -1- Enable DMA1 clock
  *        -2- Configure NVIC for DMA1 transfer complete/error interrupts
  *        -3- Configure the DMA1_Channel2_3 functional parameters
  *        -4- Configure the DMA1_Channel2_3 functional parameters
  *        -5- Enable DMA1 interrupts complete/error
  * @param   None
  * @retval  None
  */
void Configure_SPI_DMA(void)
{
    /* DMA1 used for SPI1 Transmission
     * DMA1 used for SPI1 Reception
     */
    /* (1) Enable the clock of DMA1-ch2 and DMA1-ch3 (Done in Configure_DMA())
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1); */

    /* (2) Configure NVIC for DMA transfer complete/error interrupts */
    NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0);
    NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

    /* (3) Configure the DMA1_Channel2 RX functional parameters */
    LL_DMA_ConfigTransfer(DMA1, LL_DMA_CHANNEL_2,
                          LL_DMA_DIRECTION_PERIPH_TO_MEMORY
			  | LL_DMA_PRIORITY_HIGH
			  | LL_DMA_MODE_CIRCULAR
			  | LL_DMA_PERIPH_NOINCREMENT
			  | LL_DMA_MEMORY_INCREMENT
			  | LL_DMA_PDATAALIGN_BYTE
			  | LL_DMA_MDATAALIGN_BYTE);
    LL_DMA_ConfigAddresses(DMA1,
			   LL_DMA_CHANNEL_2,
			   LL_SPI_DMA_GetRegAddr(SPI1),
			   (uint32_t)SPI_RxBuffer,
                           LL_DMA_GetDataTransferDirection(DMA1,
							   LL_DMA_CHANNEL_2));
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_2, SPI_NbDataToReceive);

    /* (4) Configure the DMA1_Channel3 TX functional parameters */
    LL_DMA_ConfigTransfer(DMA1, LL_DMA_CHANNEL_3,
                          LL_DMA_DIRECTION_MEMORY_TO_PERIPH
			  | LL_DMA_PRIORITY_HIGH
			  | LL_DMA_MODE_NORMAL
			  | LL_DMA_PERIPH_NOINCREMENT
			  | LL_DMA_MEMORY_INCREMENT
			  | LL_DMA_PDATAALIGN_BYTE
			  | LL_DMA_MDATAALIGN_BYTE);
    LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_3, (uint32_t) SPI_TxBuffer,
			   LL_SPI_DMA_GetRegAddr(SPI1),
                           LL_DMA_GetDataTransferDirection(DMA1,
							   LL_DMA_CHANNEL_3));
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, SPI_NbDataToTransmit);

    /* (5) Enable DMA interrupts complete/error */
    LL_DMA_EnableIT_HT(DMA1, LL_DMA_CHANNEL_2);
    LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_2);
    LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_2);
    LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_3);
    LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_3);
}
#endif
/**
  * @brief This function configures SPI1.
  * @note  This function is used to :\n
  *        -1- Enables GPIO clock and configures the SPI1 pins.\n
  *        -2- Configure SPI1 functional parameters.
  * @note   Peripheral configuration is minimal configuration from reset values.
  *         Thus, some useless LL unitary functions calls below are provided as
  *         commented examples - setting is default configuration from reset.
  * @param  None
  * @retval None
  */
void Configure_SPI(void)
{
    /* (1) the GPIO clock is already configured in main.c:gpio_enb()
     * the SPI pins are configured here */

    /* configure nSS */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_4, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_4, LL_GPIO_AF_0);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_4, LL_GPIO_SPEED_FREQ_VERY_HIGH);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_4, LL_GPIO_PULL_UP);

    /* Configure SCK Pin */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_5, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_5, LL_GPIO_AF_0);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_5, LL_GPIO_SPEED_FREQ_VERY_HIGH);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_5, LL_GPIO_PULL_DOWN);

    /* Configure MISO Pin */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_6, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_6, LL_GPIO_AF_0);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_6, LL_GPIO_SPEED_FREQ_VERY_HIGH);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_6, LL_GPIO_PULL_DOWN);

    /* Configure MOSI Pin */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_7, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_7, LL_GPIO_AF_0);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_7, LL_GPIO_SPEED_FREQ_VERY_HIGH);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_7, LL_GPIO_PULL_DOWN);

    /* (2) Configure SPI1 functional parameters ******************************/
    /* Enable the peripheral clock of SPI1 */
    LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_SPI1);

    /* Configure SPI1 communication */
    LL_SPI_SetBaudRatePrescaler(SPI1, LL_SPI_BAUDRATEPRESCALER_DIV64);
    LL_SPI_SetTransferDirection(SPI1,LL_SPI_FULL_DUPLEX);
    LL_SPI_SetClockPolarity(SPI1, LL_SPI_POLARITY_HIGH);
    LL_SPI_SetClockPhase(SPI1, LL_SPI_PHASE_2EDGE);
    /* Reset value is LL_SPI_MSB_FIRST */
    LL_SPI_SetTransferBitOrder(SPI1, LL_SPI_MSB_FIRST);
    LL_SPI_SetDataWidth(SPI1, LL_SPI_DATAWIDTH_8BIT);
    LL_SPI_SetNSSMode(SPI1, LL_SPI_NSS_HARD_INPUT);
    //LL_SPI_SetNSSMode(SPI1, LL_SPI_NSS_SOFT);
    LL_SPI_SetRxFIFOThreshold(SPI1, LL_SPI_RX_FIFO_TH_QUARTER);
    /* Reset value is LL_SPI_MODE_SLAVE */
    LL_SPI_SetMode(SPI1, LL_SPI_MODE_SLAVE);
    /* reset pin status */
    LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_4);
#ifdef SPI_DMA_ENB
    /* Configure SPI1 DMA transfer interrupts */
    /* Enable DMA RX Interrupt */
    LL_SPI_EnableDMAReq_RX(SPI1);
    /* Enable DMA TX Interrupt */
    LL_SPI_EnableDMAReq_TX(SPI1);
#else
    /* enable SPI1 interrupts */
    LL_SPI_EnableIT_RXNE(SPI1);
    LL_SPI_EnableIT_ERR(SPI1);
    LL_SPI_EnableIT_TXE(SPI1);

    NVIC_SetPriority(SPI1_IRQn, 0);
    NVIC_EnableIRQ(SPI1_IRQn);
#endif
}

/**
  * @brief  This function Activate SPI1
  * @param  None
  * @retval None
  */
void Activate_SPI(void)
{
    LL_SPI_EnableIT_RXNE(SPI1);
    NVIC_SetPriority(SPI1_IRQn, 0);
    NVIC_EnableIRQ(SPI1_IRQn);

    /* Enable SPI1 */
    LL_SPI_Enable(SPI1);
#ifdef SPI_DMA_ENB
    /* Enable DMA Channels */
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_2);
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
#endif
}

/******************************************************************************/
/*   USER IRQ HANDLER TREATMENT Functions                                     */
/******************************************************************************/
#ifdef SPI_DMA_ENB
/**
  * @brief  Function called from DMA1 IRQ Handler when Rx transfer is completed
  * @param  None
  * @retval None
  */
void SPI1_DMA1_ReceiveComplete_Callback(void)
{
    /* DMA Rx transfer completed */
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, SPI_TX_BUFFER_LEN);
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
}

/**
  * @brief  Function called from DMA1 IRQ Handler when Tx transfer is completed
  * @param  None
  * @retval None
  */
void SPI1_DMA1_TransmitComplete_Callback(void)
{
    /* DMA Tx transfer completed
     * pre-load the fifo for the next transaction */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_3);
}

/**
  * @brief  Function called in case of error detected in SPI IT Handler
  * @param  None
  * @retval None
  */
void SPI1_TransferError_Callback(void)
{
    /* Disable DMA1 Rx Channel */
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_2);
    /* Disable DMA1 Tx Channel */
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
    //ubDmaDisabledError = 1;
}

void fill_DMA1_SPI1TxBuffer(void)
{
    uint8_t *adp;
    uint16_t total;

    __asm__ volatile("nop");
    adp = &SPI_TxBuffer[0];
    *adp++ = 0;
    *adp++ = 0;
    
    total = ch0_total();
    *adp++ = (total >> 8) & 0x0f;
    *adp++ = total & 0x00ff;

    total = ch1_total();
    *adp++ = (total >> 8) & 0x0f;
    *adp++ = total & 0x00ff;
        
    total = temp_total();
    *adp++ = (total >> 8) & 0x0f;
    *adp++ = total & 0x00ff;

    total = SW_RELEASE;
    *adp++ = (total >> 8) & 0xff;
    *adp = total & 0x00ff;
}
#else
/**
 * Act upon incoming command
 * There is potentially a case where we receive only the first byte
 *   then must wait for more clocks.
 */
void SpiRxAndAct(void)
{
    uint16_t total;
    volatile uint16_t count;
    uint8_t cmd;

    if((count = LL_SPI_GetRxFIFOLevel(SPI1)) == LL_SPI_RX_FIFO_EMPTY)
        return;

    if(cmd == 0) {
        cmd = LL_SPI_ReceiveData8(SPI1);
        --count;
    }

    switch(cmd) {
    case CH0:
        total = ch0_total();
        break;
    case CH1:
        total = ch1_total();
        break;
    case TEMP:
        total = temp_total();
        break;
    case VREF:
        total = SW_RELEASE;     /* vref_total(); */
        break;
    default:
        total = 0;
        break;
    }

    LL_SPI_TransmitData8(SPI1, (total >> 8) & 0x0f);
    LL_SPI_TransmitData8(SPI1, total & 0x00ff);

    cmd = 0; /* after sending a response to a command, send zeros */
}

void SpiTxMt(void)
{
}

void SpiErrFlag(void)
{
    while(LL_SPI_IsActiveFlag_OVR(SPI1))
        LL_SPI_ClearFlag_OVR(SPI1);
}  
#endif
#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d", file, line) */

    /* Infinite loop */
    while(1) {
    }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
