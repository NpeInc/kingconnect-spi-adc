#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <getopt.h>
#include <string.h>

int main(int argc, char **argv)
{
    int c;
    int i;
    FILE *ifp;
    FILE *ofp;
    char *ifname;
    char *ofname;

    if(argc > 1) {
	while((c = getopt(argc, argv, "i:o?h")) != -1)
	    switch(c) {
	    case 'f':
		ifname = malloc(strlen(optarg) + 1);
		strncpy(ifname, optarg, strlen(optarg));
		printf("ifname: %s\r\n", ifname);
		ifname[strlen(optarg)] = 0;
		break;
	    case 'd':
		ofname = malloc(strlen(optarg) + 1);
		strncpy(ofname, optarg, strlen(optarg));
		ofname[strlen(optarg)] = 0;
		printf("ofname: %s\r\n", ofname);
		break;
	    default:
		break;
	    }
	ifp = fopen(ifname, "r+");
	ofp = fopen(ofname, "w+");
    } else {
    	ifp = stdin;
	ofp = stdout;
    }

    fprintf(ofp, "char ST_BL_code[] = {\n");
    //fprintf(ofp, "\tint numbytes = %x;\n", sizeof(*ifp));
    i = 0;
    while((c = getc(ifp)) != -1) {
	fprintf(ofp, "\t0x%02x,", c);
	if((i & 7) == 7)
	    printf("\n");
	++i;
    }
    fprintf(ofp, "};\n");

    fclose(ifp);
    fclose(ofp);
    return 0;
}
