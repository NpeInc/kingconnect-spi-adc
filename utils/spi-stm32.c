/**
 * \file
 *
 * encapsulate the STM32 specific commands
 */
#include "main.h"

#define PAGE	256		/**< 256 byte writes to the bootloader */

int st_read(int fd, void *byte, int num)
{
    int waittime;
    int ret;
    int len = num;
    char *b = (char *) byte;

    while(len > 0) {
	nop();
	ret = read(fd, (void*) b, 1);
	nop();
	if(ret == 0)
	    return ERR_TIMEDOUT;
	if(ret < 0)
	    return ERR_UNKNOWN;
	b += ret;
	len -= ret;
    }
    return ERR_OK;
}

int st_write(int fd, void *byte, int num)
{
    int ret;
    int len = num;
    char *b = (char *) byte;

    while(len > 0) {
	nop();
	ret = write(fd, (void*) b, 1);
	nop();
	if(ret < 1)
	    return ERR_UNKNOWN;
	len -= ret;
	b += ret;
    }
    return ERR_OK;
}

/**
 * get ack/nack from the BootLoader.
 * \param fd is the file descriptor of the device
 * \param timo is the timeout value, if non-zero time is seconds
 */
int get_ack(int fd, uint32_t timo)
{
    int ret;
    int count;
    char rdbyte;

    count = 0;
    do {
	ret = st_read(fd, &rdbyte, 1);
	if(ret == ERR_TIMEDOUT && timo != 0) {
	    usleep(1000);
	    count++;
	    if(count < timo)
		continue;
	}

	if(ret != ERR_OK)
	    return ERR_UNKNOWN;

	if(rdbyte == CMD_ACK)
	    return ERR_OK;
	if(rdbyte == CMD_NACK)
	    return ERR_NACK;
	if(rdbyte != CMD_BUSY) {
	    printf("Received 0x%02x NOT ACK!\r\n", rdbyte);
	    return ERR_UNKNOWN;
	}
    } while(count < timo);
}

int send_init_seq(int fd)
{
    int ret;
    char cmd = CMD_INIT;
    char rdbyte = 0;

    ret = st_write(fd, &cmd, 1);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;
    ret = st_read(fd, &rdbyte, 1);
    if(ret == ERR_OK && rdbyte == CMD_ACK)
	return ERR_OK;
    if(ret == ERR_OK && rdbyte == CMD_NACK) {
	printf("Port was not closed correctly\r\n");
	return ERR_OK;
    }
    if(ret != ERR_TIMEDOUT)
	return ERR_UNKNOWN;

    /* try to recover from a previous protocol error */
    ret = st_write(fd, &cmd, 1);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;
    ret = st_read(fd, &rdbyte, 1);
    if(ret == ERR_OK && rdbyte == CMD_NACK)
	return ERR_OK;
    if(ret == ERR_OK && rdbyte == CMD_ACK)
	printf("init_seq recovery return an ACK\r\n");

    return ERR_UNKNOWN;
}

int send_command(int fd, char cmd, uint32_t timo)
{
    int ret;
    uint8_t buf[4];

    buf[0] = cmd;
    buf[1] = cmd ^ 0xff;

    ret = st_write(fd, buf, 2);
    if(ret != ERR_OK) {
	printf("failed to send cmd: %x\r\n", cmd);
	return ERR_UNKNOWN;
    }

    ret = get_ack(fd, timo);
    if(ret == ERR_OK)
	return ERR_OK;
    if(ret == ERR_NACK)
	printf("Got NACK on command 0x%02x\r\n", cmd);
    else
	printf("Unexpected reply on command 0x%02x\r\n", cmd);
    return ERR_UNKNOWN;
}

int guess_len_cmd(int fd, char cmd, char *buf, unsigned int len)
{
    char *p = buf;
    unsigned int l = len;
    int ret;

    ret = send_command(fd, cmd, 0);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;

    ret = st_read(fd, p, 1);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;

    l = *p + 1;
    p++;
    ret = st_read(fd, (void*) p, l);
    if(ret == ERR_OK)
	return ERR_OK;

    return ERR_UNKNOWN;
}

int st_resync(int fd)
{
    uint8_t buf[2];
    uint8_t ack;
    uint32_t tm;
    int32_t ret;

    buf[0] = CMD_ERR;
    buf[1] = CMD_ERR ^ 0xff;

    for(tm = 0; tm < 1000; tm++) {
	ret = st_write(fd, buf, 2);
	if(ret != ERR_OK) {
	    usleep(5000);
	    continue;
	}
	ret = st_read(fd, &ack, 1);
	if(ret != ERR_OK) {
	    usleep(1000);
	    continue;
	}
	if(ack == CMD_NACK)
	    return ERR_OK;
	usleep(1000);
    }
    return ERR_UNKNOWN;
}

int st_write_mem(int32_t fd, struct st_cmd *st_cmdp, uint32_t addr,
		 uint8_t *ibuf, uint32_t len)

{
    uint8_t lbuf[258];
    int32_t ret;
    uint32_t i;
    uint8_t csum;

    if(len == 0 || len > 256) {
	printf("st_write_mem: length problem: %d\r\n", len);
	return ERR_UNKNOWN;
    }

    if(addr & 3) {
	printf("addr alignment 0x%08x\r\n", addr);
	return ERR_NO_CMD;
    }

    if(send_command(fd, st_cmdp->wm, 0) != ERR_OK)
	return ERR_UNKNOWN;

    lbuf[0] = (addr >> 24) & 0xff;
    lbuf[1] = (addr >> 16) & 0xff;
    lbuf[2] = (addr >> 8) & 0xff;
    lbuf[3] = addr & 0xff;
    lbuf[4] = lbuf[0] ^ lbuf[1] ^ lbuf[2] ^ lbuf[3];
    ret = st_write(fd, lbuf, 5);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;
    ret = get_ack(fd, 2000);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;

    len = (len + 3) & ~3;
    lbuf[0] = len - 1;
    csum = len - 1;
    for(i = 0; i < len; i++) {
	csum ^= ibuf[i];
	lbuf[i + 1] = ibuf[i];
    }
    lbuf[len + 1] = csum;
    ret = st_write(fd, lbuf, len + 2);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;

    ret = get_ack(fd, 1000);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;

    return ERR_OK;
}

int st_erase_mem(int32_t fd, struct st_cmd *st_cmdp,
		 uint32_t spage, uint32_t pages)
{
    int32_t ret;
    uint8_t buf[4];

    if(st_cmdp->er != ERASE && st_cmdp->er != EXTERA) {
	perror("NO ERASE CMD");
	return ERR_NO_CMD;
    }
    if((ret = send_command(fd, st_cmdp->er, 35000)) != ERR_OK)
	return ERR_UNKNOWN;

    if(st_cmdp->er == ERASE) {
	ret = send_command(fd, 0xff, 35000);
	if(ret != ERR_OK)
	    return ERR_UNKNOWN;
	return ERR_OK;
    }

    /* must be extended erase */
    buf[0] = 0xff;
    buf[1] = 0xff;
    buf[2] = 0x00;
    ret = st_write(fd, buf, 3);
    if(ret != ERR_OK) {
	perror("EXTERA error");
	return ERR_UNKNOWN;
    }

    ret = get_ack(fd, 45000);
    if(ret != ERR_OK) {
	perror("EXTERA GA");
	return ERR_UNKNOWN;
    }
    return ERR_OK;
}

/**
 * Write Unprotect Memory
 */
int st_wrtup_mem(int32_t fd, struct st_cmd *st_cmdp)
{
    int32_t ret;

    if(st_cmdp->uw == ERR_OK)
	return ERR_NO_CMD;

    if(send_command(fd, st_cmdp->uw, 1000) != ERR_OK)
	return ERR_UNKNOWN;

    ret = get_ack(fd, 1000);
    if(ret == ERR_NACK) {
	perror("Unprotect failure");
	return ERR_UNKNOWN;
    }

    if(ret != ERR_OK)
	return ERR_UNKNOWN;

    return ERR_OK;
}

/**
 * here it is always FLASH being rewritten.
 * \param devfd is the serial port file descriptor
 * \param st_cmdp is the supported commands in the bootloader
 * \param st_dev is the device specific information
 */
int erase_fl(int fd, struct st_cmd *st_cmdp, st_dev_t *st_dev)
{
    int32_t pagecnt;
    int32_t ret;

    pagecnt = MASS_ERASE;
    ret = st_erase_mem(fd, st_cmdp, st_dev->fl_start, pagecnt);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;
#if 0
    ret = st_wrtup_mem(fd, st_cmdp);
    if(ret != ERR_OK)
	return ERR_UNKNOWN;
    if(ret != ERR_OK) {
	printf("did not recover from the Unprotect command\r\n");
	return ERR_UNKNOWN;
    }
#endif
    return ERR_OK;
}

/**
 * here it is always FLASH being rewritten.
 * \param devfd is the serial port file descriptor
 * \param imgfd is the file containing the flash image
 * \param st_dev is the device specific information
 */
int file_wrt(int fd, int imgfd, struct st_cmd *st_cmdp, st_dev_t *st_dev)
{
    uint8_t buf[256];
    int32_t firstpage;
    int32_t pagecnt;
    int32_t ret, done;
    int32_t offset;
    int32_t size;

    done = offset = 0;
    do {
	/* read up to one page from the image file */
	if((ret = read(imgfd, buf, PAGE)) <= 0)
	    return ERR_UNKNOWN;
	if(ret < PAGE) {	/* pick up the remainer */
	    for(; ret < PAGE; ++ret)
		buf[ret] = 0xff;
	    done = 1;
	}
	ret = st_write_mem(fd, st_cmdp, st_dev->fl_start + offset, buf, PAGE);
	if(ret != ERR_OK) {
	    printf("Write at address 0x%08x failed\r\n",
		   st_dev->fl_start + offset);
	    return ERR_UNKNOWN;
	}
	offset += PAGE;
    } while(!done);

    return ERR_OK;
}

/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 */
