/**
 * \file
 *
 * header file for this application
 */
#ifndef _MAIN_H_

#define _MAIN_H_

#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <getopt.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define RESYNC_TIMEOUT		35	/**< seconds */
#define MASSERASE_TIMEOUT	35	/**< seconds */
#define PAGEERASE_TIMEOUT	5	/**< seconds */
#define BLKWRITE_TIMEOUT	1	/**< seconds */
#define WUNPROT_TIMEOUT		1	/**< seconds */
#define WPROT_TIMEOUT		1	/**< seconds */
#define RPROT_TIMEOUT		1	/**< seconds */

#define CMD_GET_LENGTH		17	/**< bytes in the reply */
#define CMD_ERR			0xff	/**< force error */

/**
 */
#define CMD_INIT	0x7f	/**< initial bytes to get the
				 * BootLoaders attention */
#define CMD_ACK		0x79	/**< command ACK byte */
#define CMD_NACK	0x1f	/**< command no-ACK byte */
#define CMD_BUSY	0x76	/**< BL busy */

/**
 * commands supported by the BootLoader
 */
#define GET	0x00	/**< get BL version and allowed commands */
#define GETVR	0x01	/**< get BL version and Rd Protection Status */
#define GETID	0x02	/**< get the chip ID */
#define RDMEM	0x11	/**< reads up to 256 bytes of memory at addr */
#define GO	0x21	/**< jump to the user app code in
			 * internal FLASH or SRAM */
#define WRMEM	0x31	/**< Write up to 256 bytes starting at addr */
#define ERASE	0x43	/**< erase from one to all Flash memory pages */
#define EXTERA	0x44	/**< erase from one to all flash - v3.0 */
#define WRTPROT	0x63	/**< enables write protection */
#define WRTUNP	0x73	/**< disable write protection */
#define RDPROT	0x82	/**< enables read protection */
#define RDUNP	0x92	/**< disable read protection */

#define MASS_ERASE	0x00100000	/**< double the highest */

#define nop()	asm __volatile__("nop")

typedef enum {
    ERR_OK = 0,
    ERR_UNKNOWN,	/**< Generic error */
    ERR_NACK,
    ERR_NO_CMD,		/**< Command not available in bootloader */
    ERR_NODEV,		/* No such device */
    ERR_TIMEDOUT	/* Operation timed out */
} stm32_err_t;

/**
 * supported commands of the BootLoader
 */
struct st_cmd {
    uint8_t get;	/**< GET - 0x00 */
    uint8_t gvr;	/**< GETVR - 0x01 */
    uint8_t gid;	/**< GETID - 0x02 */
    uint8_t rm;		/**< RDMEM - 0x11 */
    uint8_t go;		/**< GO - 0x21 */
    uint8_t wm;		/**< WRMEM - 0x31 */
    uint8_t er;		/**< 0x43 or 0x44 - this may be extended erase */
    uint8_t wp;		/**< WRTPROT - 0x63 */
    uint8_t uw;		/**< WRTUNP - 0x73 */
    uint8_t rp;		/**< RDPROT - 0x82 */
    uint8_t ur;		/**< RDUNP - 0x92 */
};

/**
 * This is the part found - provided by the BootLoader
 */
struct st_part {
    uint8_t	bl_version;	/**< BootLoader Version */
    uint8_t	version;	/**< version */
    uint8_t	option1;	/**< protection */
    uint8_t	option2;	/**< protection */
    uint16_t	pid;		/**< match to Device ID */
};

/**
 * Information for reprogramming the ST devices.
 * See dev_table.c
 */
typedef struct st_dev {
    uint16_t	id;
    const char	*name;
    uint32_t	ram_start;
    uint32_t	ram_end;
    uint32_t	fl_start;
    uint32_t	fl_end;
    uint16_t	fl_pps;		/**< pages per sector */
    uint32_t	*fl_ps;		/**< page size */
    uint32_t	opt_start;
    uint32_t	opt_end;
    uint32_t	mem_start;
    uint32_t	mem_end;
    uint32_t	flags;
} st_dev_t;

/** flags */
enum {
    F_NO_ME = 1,
    F_OBLL = 2
};

/** externals */
extern int st_read(int handle, void *byte, int num);
extern int st_write(int handle, void *byte, int num);
extern int get_ack(int handle, uint32_t timo);
extern int send_init_seq(int handle);
extern int send_init_seq(int handle);
extern int send_command(int handle, char cmd, uint32_t timo);
extern int guess_len_cmd(int handle, char cmd, char *buf, uint32_t len);
extern st_dev_t *get_devp(unsigned short pid);
extern int erase_fl(int fd, struct st_cmd *st_cmdp, st_dev_t *st_dev);
extern int file_wrt(int devfd, int imgfd,
		    struct st_cmd *st_cmdp, st_dev_t *st_dev);

#endif
/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 */
