#include <stdlib.h>
#include <stdio.h>

char outc[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

int main(int argc, char **argv)
{
    int total;
    int i;
    int j;
    
    if(argc < 2) {
    	printf("enter a total number to generate\r\n");
	return -1;
    }

    total = atoi(argv[1]);

    for(i = 0; i < total;) {
    	for(j = 0; j < 80; j++) 
	    if(j < 26)
		putchar(outc[j]);
	    else if(j < 52)
	    	putchar(outc[j - 26]);
	    else if(j < 78)
	    	putchar(outc[j - 52]);
	    else putchar(outc[j - 78]);
	i += j;
    }

    return 0;
}
