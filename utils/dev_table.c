/*
  stm32flash - Open Source ST STM32 flash program for *nix
  Copyright (C) 2010 Geoffrey McRae <geoff@spacevs.com>
  Copyright (C) 2014-2015 Antonio Borneo <borneo.antonio@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "main.h"

#define SZ_128	0x00000080
#define SZ_256	0x00000100
#define SZ_1K	0x00000400
#define SZ_2K	0x00000800
#define SZ_16K	0x00004000
#define SZ_32K	0x00008000
#define SZ_64K	0x00010000
#define SZ_128K	0x00020000
#define SZ_256K	0x00040000

/*
 * Page-size for page-by-page flash erase.
 * Arrays are zero terminated; last non-zero value is automatically repeated
 */

/* fixed size pages */
static uint32_t p_128[] = { SZ_128, 0 };
static uint32_t p_256[] = { SZ_256, 0 };
static uint32_t p_1k[]  = { SZ_1K,  0 };
static uint32_t p_2k[]  = { SZ_2K,  0 };
/* F2 and F4 page size */
static uint32_t f2f4[]  = { SZ_16K, SZ_16K, SZ_16K, SZ_16K
			    , SZ_64K, SZ_128K, 0 };
/* F4 dual bank page size */
static uint32_t f4db[]  = {
	SZ_16K, SZ_16K, SZ_16K, SZ_16K, SZ_64K, SZ_128K, SZ_128K, SZ_128K,
	SZ_16K, SZ_16K, SZ_16K, SZ_16K, SZ_64K, SZ_128K, 0
};
/* F7 page size */
static uint32_t f7[]    = { SZ_32K, SZ_32K, SZ_32K, SZ_32K,
			    SZ_128K, SZ_256K, 0 };

/*
 * Device table, corresponds to the "Bootloader device-dependant parameters"
 * table in ST document AN2606.
 * Note that the option bytes upper range is inclusive!
 */
st_dev_t st_device[] = {
    /* ID			"name"
       SRAM-address-range
       FLASH-address-range
       PPS			PSize
       Option-byte-addr-range
       System-mem-addr-range   Flags */
    /* F0 */
    {
	0x440,		"STM32F030x8/F05xxx",
	0x20000800,	0x20002000,
	0x08000000,	0x08010000,
	4,		p_1k,
	0x1ffff800,	0x1ffff80f,
	0x1fffec00,	0x1ffff800,	0
    },
    {
	0x442,		"STM32F030xC/F09xxx",
	0x20001800,	0x20008000,
	0x08000000,	0x08040000,
	2,		p_2k,
	0x1ffff800,	0x1ffff80f,
	0x1fffc800,	0x1ffff800,	F_OBLL
    },
    {
	0x444,		"STM32F03xx4/6",
	0x20000800,	0x20001000,
	0x08000000,	0x08008000,
	4,		p_1k,
	0x1ffff800,	0x1ffff80f,
	0x1fffec00,	0x1ffff800,	0
    },
#if 0
    /* F4 */
    {
	0x413,		"STM32F40xxx/41xxx",
	0x20003000,	0x20020000,
	0x08000000,	0x08100000,
	1,		f2f4,
	0x1fffc000,	0x1fffc00f,
	0x1fff0000,	0x1fff7800,	0
    },
    {
	0x423,		"STM32F401xB(C)",
	0x20003000,	0x20010000,
	0x08000000,	0x08040000,
	1,		f2f4,
	0x1fffc000,	0x1fffc00f,
	0x1fff0000,	0x1fff7800,	0
    },
    {
	0x433,		"STM32F401xD(E)",
	0x20003000,	0x20018000,
	0x08000000,	0x08080000,
	1,		f2f4,
	0x1fffc000,	0x1fffc00f,
	0x1fff0000,	0x1fff7800,	0
    },
    {
	0x431,		"STM32F411xx",
	0x20003000,	0x20020000,
	0x08000000,	0x08080000,
	1,		f2f4,
	0x1FFFC000,	0x1FFFC00F,
	0x1FFF0000,	0x1FFF7800,	0
    },
#endif
    {	0x0	}
};

int st_dev_size = sizeof(st_device) / sizeof(st_device[0]);

st_dev_t *get_devp(unsigned short pid)
{
    int i;
    for(i = 0; i < st_dev_size; i++)
	if(st_device[i].id == pid)
	    return &st_device[i];
    return NULL;
}

/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 */
