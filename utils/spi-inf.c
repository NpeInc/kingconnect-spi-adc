/**
 * \file
 *
 * \brief a test program to communicate with the STM32030F4 boot
 *	  loader.
 */
#include "main.h"

/** prototypes */
void print_results(struct st_part *st_blinfo, struct st_cmd *st_cmdp);

/**
 * print the usage 
 */
void usage(char *name)
{
    printf("\r\nUsage: %s\r\n", name);
    printf("\t-f\t\tname of file with new image\r\n");
    printf("\t-d\t\tcomm interface to open\r\n");
    printf("\t-a\t\tstart address to write\r\n");
    printf("\t-p\t\tprogram the flash\r\n");
    printf("\t-e\t\terase the flash\r\n");
    printf("\r\n");
}

#define BUFS	32	/**< general usage buffer */
uint8_t buf[BUFS];

struct termios ttyport;
struct st_cmd st_cmds;
struct st_part st_blinfo;

int main(int argc, char **argv)
{
    int ret;
    int c;
    char *p;
    int len, cnt, ptr;
    char *fname;	/**< name of file containing new image */
    char *dname;	/**< name of programming device */
    int startaddr;	/**< address in the device */
    int doprog;		/**< program or just info */
    int doerase;	/**< erase flash */
    int devfd;		/**< device file handle */
    int imgfd;		/**< binary image file handle */
    int found;
    st_dev_t *st_dev;
    struct st_cmd *st_cmdp;

    if(argc < 4) {
	usage(argv[0]);
	return -1;
    }

    doerase = doprog = 0;
    while((c = getopt(argc, argv, "d:f:a:?hpe")) != -1)
	switch(c) {
	case 'f':
	    fname = malloc(strlen(optarg) + 1);
	    strncpy(fname, optarg, strlen(optarg));
	    printf("fname: %s\r\n", fname);
	    fname[strlen(optarg)] = 0;
	    break;
	case 'd':
	    dname = malloc(strlen(optarg) + 1);
	    strncpy(dname, optarg, strlen(optarg));
	    dname[strlen(optarg)] = 0;
	    printf("dname: %s\r\n", dname);
	    break;
	case 'a':
	    startaddr = strtol(optarg, (char**) 0, 0);
	    printf("start: 0x%04x - %d offset\r\n", startaddr, startaddr);
	    break;
	case 'p':
	    doprog = 1;
	    break;
	case 'e':
	    doerase = 1;
	    break;
	default:
	case 'h':
	case '?':
	    usage(argv[0]);
	    break;
	}

    devfd = open(dname, O_NDELAY | O_RDWR | O_NOCTTY);
    if(devfd < 0) {
	perror("OPEN device");
	goto out1;
    }
    fcntl(devfd, F_SETFL, 0);

    /* set the baud, character size, parity, etc */
    ret = tcgetattr(devfd, &ttyport);
    if(ret < 0) {
	perror("tcgetattr");
	goto out2;
    }
    /* for this we will need raw reads and writes */
    cfmakeraw(&ttyport);
    /* set baud rate low */
    ttyport.c_cflag &= ~(CRTSCTS | CSIZE | CBAUD);
    ttyport.c_iflag &= ~(IXON | IXOFF | IXANY | IGNPAR);
    ttyport.c_lflag &= ~(ECHOK | ECHOCTL | ECHOKE);
    ttyport.c_oflag &= ~(OPOST | ONLCR);

    ttyport.c_cflag |= CS8 | PARENB | INPCK;	/* 8e1 */
    ttyport.c_cc[VMIN] = 0;
    ttyport.c_cc[VTIME] = 5;
    cfsetspeed(&ttyport, B9600);
    ret = tcsetattr(devfd, TCSANOW, &ttyport);
    printf("tcsetattr: %d\r\n", ret);
#if 0
    {
	uint8_t mybuf[80];
	printf("SETTINGS are:\r\n");
	p = strcpy(mybuf, "stty -a -F ");
	p += strlen(mybuf);
	strcpy(p, dname);
	system(mybuf);
	printf("\n");

	printf("iflag[%04x] oflag[%04x] cflag[%04x] lflag[%04x]\r\n",
	       ttyport.c_iflag, ttyport.c_oflag,
	       ttyport.c_cflag, ttyport.c_lflag);
	printf("c_cc: ");
	for(c = 0; c < NCCS;) {
	    printf("%o ", ttyport.c_cc[c]);
	    ++c;
	    if((c & 7) == 0) printf("\r\n");
	}
	printf("ispeed[0x%x] ospeed[0x%x]\r\n",
	       cfgetispeed(&ttyport), cfgetospeed(&ttyport));
    }
#endif
    /* open the image file */
    imgfd = open(fname, O_RDONLY);
    if(imgfd < 0) {
	perror("OPEN image file");
	goto out2;
    }

    /* TODO:
       assert boot0, then de-assert nRST, assert nRST */

    ret = send_init_seq(devfd);
    if(ret != ERR_OK) {
	perror("send_init_seq");
	goto out3;
    }

    /* get version and read-protection status */
    ret = send_command(devfd, GETVR, 0);
    if(ret != ERR_OK) {
	perror("GETVR cmd");
	goto out3;
    }

    len = 3;
    ret = st_read(devfd, buf, len);
    if(ret != ERR_OK) {
	perror("GETVR read");
	goto out3;
    }
    st_blinfo.version = buf[0];
    st_blinfo.option1 = buf[1];
    st_blinfo.option2 = buf[2];

    /* try to get the ACK */
    ret = get_ack(devfd, 0);
    if(ret != ERR_OK && ret != ERR_NACK) {
	perror("get_ack(1)");
	goto out3;
    }

    /* get the BL info */
    len = CMD_GET_LENGTH;
    ret = guess_len_cmd(devfd, GET, buf, len);
    if(ret != ERR_OK) {
	perror("GET");
	goto out3;
    }
    if(get_ack(devfd, 0) != ERR_OK) {
	printf("get_ack(2)");
	goto out3;
    }
    st_cmdp = &st_cmds;
    len = buf[0] + 1;
    st_blinfo.bl_version = buf[1];	/* go figure ?? */
    st_cmdp->get = buf[2];
    st_cmdp->gvr = buf[3];
    st_cmdp->gid = buf[4];
    st_cmdp->rm = buf[5];
    st_cmdp->go = buf[6];
    st_cmdp->wm = buf[7];
    st_cmdp->er = buf[8];
    st_cmdp->wp = buf[9];
    st_cmdp->uw = buf[10];
    st_cmdp->rp = buf[11];
    st_cmdp->ur = buf[12];

    /* get chip id */
    ret = guess_len_cmd(devfd, st_cmdp->gid, buf, 1);
    if(ret != ERR_OK) {
	perror("GetChipID");
	goto out3;
    }
    len = buf[0] + 1;
    st_blinfo.pid = (buf[1] << 8) | buf[2];
    if(get_ack(devfd, 2000) != ERR_OK) {
	perror("get_ack(3)");
	goto out3;
    }

    if(doprog == 0 && doerase == 0) {
	print_results(&st_blinfo, st_cmdp);
	goto out3;
    }

    if(doprog == 1 && doerase == 1) {
	printf("Specify erase OR program\r\n");
	goto out3;
    }

    st_dev = get_devp(st_blinfo.pid);
    if(st_dev == NULL)
	goto out3;

    if(doprog)
	ret = file_wrt(devfd, imgfd, st_cmdp, st_dev);
    if(doerase) {
	ret = erase_fl(devfd, st_cmdp, st_dev);
	if(ret == ERR_OK)
	    printf("Erase succeeded - now cycle power before programming\r\n");
    }

    if(ret == ERR_OK)
	printf("Reprogram succeeded\r\n");
    else
	printf("Program rewrite failure: %d\r\n", ret);

 out3:
    close(imgfd);
 out2:
    close(devfd);
 out1:
    free(dname);
    free(fname);
    return 0;
}

/**
 * show the results given by the bootloader
 */
void print_results(struct st_part *st_blinfo, struct st_cmd *st_cmdp)
{
    if(st_blinfo == NULL || st_cmdp == NULL)
	return;

    printf("Version: 0x%02x\t\tDevID: 0x%04x\r\n",
	   st_blinfo->version, st_blinfo->pid);
    printf("Option1: 0x%02x\t\tOption2: 0x%02x\r\n",
	   st_blinfo->option1, st_blinfo->option2);

    printf("GET: 0x%02x\t\tGVR: 0x%02x\t\tGID: 0x%02x\r\n",
	   st_cmdp->get, st_cmdp->gvr, st_cmdp->gid);
    printf("RM: 0x%02x\t\tGO: 0x%02x\t\tWM: 0x%02x\r\n",
	   st_cmdp->rm, st_cmdp->go, st_cmdp->wm);
    printf("ER: 0x%02x\t\tWP: 0x%02x\t\tUW: 0x%02x\r\n",
	   st_cmdp->er, st_cmdp->wp, st_cmdp->uw);
    printf("RP: 0x%02x\t\tUR: 0x%02x\r\n",
	   st_cmdp->rp, st_cmdp->ur);
}

/*
 * Local Variables:
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 */
